﻿#region License

/*
MIT License
Copyright © 2006 The Mono.Xna Team

All rights reserved.

Authors
 * Alan McGovern

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#endregion License

using System;

namespace FixedPointMath
{

	[Serializable]
	public struct FVector2 : IEquatable<FVector2>
	{
		#region Private Fields

		private static FVector2 zeroVector = new FVector2(0, 0);
		private static FVector2 oneVector = new FVector2(1, 1);

		private static FVector2 rightVector = new FVector2(1, 0);
		private static FVector2 leftVector = new FVector2(-1, 0);

		private static FVector2 upVector = new FVector2(0, 1);
		private static FVector2 downVector = new FVector2(0, -1);

		#endregion Private Fields

		#region Public Fields

		public Fixed x;
		public Fixed y;

		#endregion Public Fields

		#region Properties

		public static FVector2 zero
		{
			get { return zeroVector; }
		}

		public static FVector2 one
		{
			get { return oneVector; }
		}

		public static FVector2 right
		{
			get { return rightVector; }
		}

		public static FVector2 left
		{
			get { return leftVector; }
		}

		public static FVector2 up
		{
			get { return upVector; }
		}

		public static FVector2 down
		{
			get { return downVector; }
		}

		#endregion Properties

		#region Constructors

		/// <summary>
		/// Constructor foe standard 2D vector.
		/// </summary>
		/// <param name="x">
		/// A <see cref="System.Single"/>
		/// </param>
		/// <param name="y">
		/// A <see cref="System.Single"/>
		/// </param>
		public FVector2(Fixed x, Fixed y)
		{
			this.x = x;
			this.y = y;
		}

		/// <summary>
		/// Constructor for "square" vector.
		/// </summary>
		/// <param name="value">
		/// A <see cref="System.Single"/>
		/// </param>
		public FVector2(Fixed value)
		{
			x = value;
			y = value;
		}

		public void Set(Fixed x, Fixed y)
		{
			this.x = x;
			this.y = y;
		}

		#endregion Constructors

		#region Public Methods

		public static void Reflect(ref FVector2 vector, ref FVector2 normal, out FVector2 result)
		{
			Fixed dot = Dot(vector, normal);
			result.x = vector.x - ((2f * dot) * normal.x);
			result.y = vector.y - ((2f * dot) * normal.y);
		}

		public static FVector2 Reflect(FVector2 vector, FVector2 normal)
		{
			FVector2 result;
			Reflect(ref vector, ref normal, out result);
			return result;
		}

		public static FVector2 Add(FVector2 value1, FVector2 value2)
		{
			value1.x += value2.x;
			value1.y += value2.y;
			return value1;
		}

		public static void Add(ref FVector2 value1, ref FVector2 value2, out FVector2 result)
		{
			result.x = value1.x + value2.x;
			result.y = value1.y + value2.y;
		}

		public static FVector2 Barycentric(FVector2 value1, FVector2 value2, FVector2 value3, Fixed amount1, Fixed amount2)
		{
			return new FVector2(
				FMath.Barycentric(value1.x, value2.x, value3.x, amount1, amount2),
				FMath.Barycentric(value1.y, value2.y, value3.y, amount1, amount2));
		}

		public static void Barycentric(ref FVector2 value1, ref FVector2 value2, ref FVector2 value3, Fixed amount1,
									   Fixed amount2, out FVector2 result)
		{
			result = new FVector2(
				FMath.Barycentric(value1.x, value2.x, value3.x, amount1, amount2),
				FMath.Barycentric(value1.y, value2.y, value3.y, amount1, amount2));
		}

		public static FVector2 CatmullRom(FVector2 value1, FVector2 value2, FVector2 value3, FVector2 value4, Fixed amount)
		{
			return new FVector2(
				FMath.CatmullRom(value1.x, value2.x, value3.x, value4.x, amount),
				FMath.CatmullRom(value1.y, value2.y, value3.y, value4.y, amount));
		}

		public static void CatmullRom(ref FVector2 value1, ref FVector2 value2, ref FVector2 value3, ref FVector2 value4,
									  Fixed amount, out FVector2 result)
		{
			result = new FVector2(
				FMath.CatmullRom(value1.x, value2.x, value3.x, value4.x, amount),
				FMath.CatmullRom(value1.y, value2.y, value3.y, value4.y, amount));
		}

		public static FVector2 Clamp(FVector2 value1, FVector2 min, FVector2 max)
		{
			return new FVector2(
				FMath.Clamp(value1.x, min.x, max.x),
				FMath.Clamp(value1.y, min.y, max.y));
		}

		public static void Clamp(ref FVector2 value1, ref FVector2 min, ref FVector2 max, out FVector2 result)
		{
			result = new FVector2(
				FMath.Clamp(value1.x, min.x, max.x),
				FMath.Clamp(value1.y, min.y, max.y));
		}

		/// <summary>
		/// Returns Fixed precison distanve between two vectors
		/// </summary>
		/// <param name="value1">
		/// A <see cref="FVector2"/>
		/// </param>
		/// <param name="value2">
		/// A <see cref="FVector2"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Single"/>
		/// </returns>
		public static Fixed Distance(FVector2 value1, FVector2 value2)
		{
			Fixed result;
			DistanceSquared(ref value1, ref value2, out result);
			return (Fixed)Fixed.Sqrt(result);
		}


		public static void Distance(ref FVector2 value1, ref FVector2 value2, out Fixed result)
		{
			DistanceSquared(ref value1, ref value2, out result);
			result = (Fixed)Fixed.Sqrt(result);
		}

		public static Fixed DistanceSquared(FVector2 value1, FVector2 value2)
		{
			Fixed result;
			DistanceSquared(ref value1, ref value2, out result);
			return result;
		}

		public static void DistanceSquared(ref FVector2 value1, ref FVector2 value2, out Fixed result)
		{
			result = (value1.x - value2.x) * (value1.x - value2.x) + (value1.y - value2.y) * (value1.y - value2.y);
		}

		/// <summary>
		/// Devide first vector with the secund vector
		/// </summary>
		/// <param name="value1">
		/// A <see cref="FVector2"/>
		/// </param>
		/// <param name="value2">
		/// A <see cref="FVector2"/>
		/// </param>
		/// <returns>
		/// A <see cref="FVector2"/>
		/// </returns>
		public static FVector2 Divide(FVector2 value1, FVector2 value2)
		{
			value1.x /= value2.x;
			value1.y /= value2.y;
			return value1;
		}

		public static void Divide(ref FVector2 value1, ref FVector2 value2, out FVector2 result)
		{
			result.x = value1.x / value2.x;
			result.y = value1.y / value2.y;
		}

		public static FVector2 Divide(FVector2 value1, Fixed divider)
		{
			Fixed factor = 1 / divider;
			value1.x *= factor;
			value1.y *= factor;
			return value1;
		}

		public static void Divide(ref FVector2 value1, Fixed divider, out FVector2 result)
		{
			Fixed factor = 1 / divider;
			result.x = value1.x * factor;
			result.y = value1.y * factor;
		}

		public static Fixed Dot(FVector2 value1, FVector2 value2)
		{
			return value1.x * value2.x + value1.y * value2.y;
		}

		public static void Dot(ref FVector2 value1, ref FVector2 value2, out Fixed result)
		{
			result = value1.x * value2.x + value1.y * value2.y;
		}

		public override bool Equals(object obj)
		{
			return (obj is FVector2) ? this == ((FVector2)obj) : false;
		}

		public bool Equals(FVector2 other)
		{
			return this == other;
		}

		public override int GetHashCode()
		{
			return (int)(x + y);
		}

		public static FVector2 Hermite(FVector2 value1, FVector2 tangent1, FVector2 value2, FVector2 tangent2, Fixed amount)
		{
			FVector2 result = new FVector2();
			Hermite(ref value1, ref tangent1, ref value2, ref tangent2, amount, out result);
			return result;
		}

		public static void Hermite(ref FVector2 value1, ref FVector2 tangent1, ref FVector2 value2, ref FVector2 tangent2,
								   Fixed amount, out FVector2 result)
		{
			result.x = FMath.Hermite(value1.x, tangent1.x, value2.x, tangent2.x, amount);
			result.y = FMath.Hermite(value1.y, tangent1.y, value2.y, tangent2.y, amount);
		}

		public Fixed magnitude
		{
			get
			{
				Fixed result;
				DistanceSquared(ref this, ref zeroVector, out result);
				return Fixed.Sqrt(result);
			}
		}

		public static FVector2 ClampMagnitude(FVector2 vector, Fixed maxLength)
		{
			return Normalize(vector) * maxLength;
		}

		public Fixed LengthSquared()
		{
			Fixed result;
			DistanceSquared(ref this, ref zeroVector, out result);
			return result;
		}

		public static FVector2 Lerp(FVector2 value1, FVector2 value2, Fixed amount)
		{
			amount = FMath.Clamp(amount, 0, 1);

			return new FVector2(
				FMath.Lerp(value1.x, value2.x, amount),
				FMath.Lerp(value1.y, value2.y, amount));
		}

		public static FVector2 LerpUnclamped(FVector2 value1, FVector2 value2, Fixed amount)
		{
			return new FVector2(
				FMath.Lerp(value1.x, value2.x, amount),
				FMath.Lerp(value1.y, value2.y, amount));
		}

		public static void LerpUnclamped(ref FVector2 value1, ref FVector2 value2, Fixed amount, out FVector2 result)
		{
			result = new FVector2(
				FMath.Lerp(value1.x, value2.x, amount),
				FMath.Lerp(value1.y, value2.y, amount));
		}

		public static FVector2 Max(FVector2 value1, FVector2 value2)
		{
			return new FVector2(
				FMath.Max(value1.x, value2.x),
				FMath.Max(value1.y, value2.y));
		}

		public static void Max(ref FVector2 value1, ref FVector2 value2, out FVector2 result)
		{
			result.x = FMath.Max(value1.x, value2.x);
			result.y = FMath.Max(value1.y, value2.y);
		}

		public static FVector2 Min(FVector2 value1, FVector2 value2)
		{
			return new FVector2(
				FMath.Min(value1.x, value2.x),
				FMath.Min(value1.y, value2.y));
		}

		public static void Min(ref FVector2 value1, ref FVector2 value2, out FVector2 result)
		{
			result.x = FMath.Min(value1.x, value2.x);
			result.y = FMath.Min(value1.y, value2.y);
		}

		public void Scale(FVector2 other)
		{
			this.x = x * other.x;
			this.y = y * other.y;
		}

		public static FVector2 Scale(FVector2 value1, FVector2 value2)
		{
			FVector2 result;
			result.x = value1.x * value2.x;
			result.y = value1.y * value2.y;

			return result;
		}

		public static FVector2 Multiply(FVector2 value1, FVector2 value2)
		{
			value1.x *= value2.x;
			value1.y *= value2.y;
			return value1;
		}

		public static FVector2 Multiply(FVector2 value1, Fixed scaleFactor)
		{
			value1.x *= scaleFactor;
			value1.y *= scaleFactor;
			return value1;
		}

		public static void Multiply(ref FVector2 value1, Fixed scaleFactor, out FVector2 result)
		{
			result.x = value1.x * scaleFactor;
			result.y = value1.y * scaleFactor;
		}

		public static void Multiply(ref FVector2 value1, ref FVector2 value2, out FVector2 result)
		{
			result.x = value1.x * value2.x;
			result.y = value1.y * value2.y;
		}

		public static FVector2 Negate(FVector2 value)
		{
			value.x = -value.x;
			value.y = -value.y;
			return value;
		}

		public static void Negate(ref FVector2 value, out FVector2 result)
		{
			result.x = -value.x;
			result.y = -value.y;
		}

		public void Normalize()
		{
			Normalize(ref this, out this);
		}

		public static FVector2 Normalize(FVector2 value)
		{
			Normalize(ref value, out value);
			return value;
		}

		public FVector2 normalized
		{
			get
			{
				FVector2 result;
				FVector2.Normalize(ref this, out result);

				return result;
			}
		}

		public static void Normalize(ref FVector2 value, out FVector2 result)
		{
			Fixed factor;
			DistanceSquared(ref value, ref zeroVector, out factor);
			factor = 1f / (Fixed)Fixed.Sqrt(factor);
			result.x = value.x * factor;
			result.y = value.y * factor;
		}

		public static FVector2 SmoothStep(FVector2 value1, FVector2 value2, Fixed amount)
		{
			return new FVector2(
				FMath.SmoothStep(value1.x, value2.x, amount),
				FMath.SmoothStep(value1.y, value2.y, amount));
		}

		public static void SmoothStep(ref FVector2 value1, ref FVector2 value2, Fixed amount, out FVector2 result)
		{
			result = new FVector2(
				FMath.SmoothStep(value1.x, value2.x, amount),
				FMath.SmoothStep(value1.y, value2.y, amount));
		}

		public static FVector2 Subtract(FVector2 value1, FVector2 value2)
		{
			value1.x -= value2.x;
			value1.y -= value2.y;
			return value1;
		}

		public static void Subtract(ref FVector2 value1, ref FVector2 value2, out FVector2 result)
		{
			result.x = value1.x - value2.x;
			result.y = value1.y - value2.y;
		}

		public static Fixed Angle(FVector2 a, FVector2 b)
		{
			return Fixed.Acos(a.normalized * b.normalized) * Fixed.Rad2Deg;
		}

		public FVector3 ToFVector()
		{
			return new FVector3(this.x, this.y, 0);
		}

		public override string ToString()
		{
			return string.Format("({0:f1}, {1:f1})", x.AsFloat(), y.AsFloat());
		}

		#endregion Public Methods

		#region Operators

		public static FVector2 operator -(FVector2 value)
		{
			value.x = -value.x;
			value.y = -value.y;
			return value;
		}


		public static bool operator ==(FVector2 value1, FVector2 value2)
		{
			return value1.x == value2.x && value1.y == value2.y;
		}


		public static bool operator !=(FVector2 value1, FVector2 value2)
		{
			return value1.x != value2.x || value1.y != value2.y;
		}


		public static FVector2 operator +(FVector2 value1, FVector2 value2)
		{
			value1.x += value2.x;
			value1.y += value2.y;
			return value1;
		}


		public static FVector2 operator -(FVector2 value1, FVector2 value2)
		{
			value1.x -= value2.x;
			value1.y -= value2.y;
			return value1;
		}


		public static Fixed operator *(FVector2 value1, FVector2 value2)
		{
			return FVector2.Dot(value1, value2);
		}


		public static FVector2 operator *(FVector2 value, Fixed scaleFactor)
		{
			value.x *= scaleFactor;
			value.y *= scaleFactor;
			return value;
		}


		public static FVector2 operator *(Fixed scaleFactor, FVector2 value)
		{
			value.x *= scaleFactor;
			value.y *= scaleFactor;
			return value;
		}


		public static FVector2 operator /(FVector2 value1, FVector2 value2)
		{
			value1.x /= value2.x;
			value1.y /= value2.y;
			return value1;
		}


		public static FVector2 operator /(FVector2 value1, Fixed divider)
		{
			Fixed factor = 1 / divider;
			value1.x *= factor;
			value1.y *= factor;
			return value1;
		}

		#endregion Operators
	}
}
