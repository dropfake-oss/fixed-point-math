﻿/* Copyright (C) <2009-2011> <Thorben Linneweber, Jitter Physics>
*
*  This software is provided 'as-is', without any express or implied
*  warranty.  In no event will the authors be held liable for any damages
*  arising from the use of this software.
*
*  Permission is granted to anyone to use this software for any purpose,
*  including commercial applications, and to alter it and redistribute it
*  freely, subject to the following restrictions:
*
*  1. The origin of this software must not be misrepresented; you must not
*      claim that you wrote the original software. If you use this software
*      in a product, an acknowledgment in the product documentation would be
*      appreciated but is not required.
*  2. Altered source versions must be plainly marked as such, and must not be
*      misrepresented as being the original software.
*  3. This notice may not be removed or altered from any source distribution.
*/

using System;

namespace FixedPointMath
{

	/// <summary>
	/// A Quaternion representing an orientation.
	/// </summary>
	[Serializable]
	public struct FQuaternion
	{

		/// <summary>The X component of the quaternion.</summary>
		public Fixed x;
		/// <summary>The Y component of the quaternion.</summary>
		public Fixed y;
		/// <summary>The Z component of the quaternion.</summary>
		public Fixed z;
		/// <summary>The W component of the quaternion.</summary>
		public Fixed w;

		public static readonly FQuaternion identity;
		public static readonly FQuaternion MaxValue;

		static FQuaternion()
		{
			identity = new FQuaternion(0, 0, 0, 1);
			MaxValue = new FQuaternion(Fixed.MaxValue, Fixed.MaxValue, Fixed.MaxValue, Fixed.MaxValue);
		}

		/// <summary>
		/// Initializes a new instance of the JQuaternion structure.
		/// </summary>
		/// <param name="x">The X component of the quaternion.</param>
		/// <param name="y">The Y component of the quaternion.</param>
		/// <param name="z">The Z component of the quaternion.</param>
		/// <param name="w">The W component of the quaternion.</param>
		public FQuaternion(Fixed x, Fixed y, Fixed z, Fixed w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public void Set(Fixed new_x, Fixed new_y, Fixed new_z, Fixed new_w)
		{
			this.x = new_x;
			this.y = new_y;
			this.z = new_z;
			this.w = new_w;
		}

		public void SetFromToRotation(FVector3 fromDirection, FVector3 toDirection)
		{
			FQuaternion targetRotation = FQuaternion.FromToRotation(fromDirection, toDirection);
			this.Set(targetRotation.x, targetRotation.y, targetRotation.z, targetRotation.w);
		}

		public FVector3 eulerAngles
		{
			get
			{
				FVector3 result = new FVector3();

				Fixed ysqr = y * y;
				Fixed t0 = -2.0f * (ysqr + z * z) + 1.0f;
				Fixed t1 = +2.0f * (x * y - w * z);
				Fixed t2 = -2.0f * (x * z + w * y);
				Fixed t3 = +2.0f * (y * z - w * x);
				Fixed t4 = -2.0f * (x * x + ysqr) + 1.0f;

				t2 = t2 > 1.0f ? 1.0f : t2;
				t2 = t2 < -1.0f ? -1.0f : t2;

				result.x = Fixed.Atan2(t3, t4) * Fixed.Rad2Deg;
				result.y = Fixed.Asin(t2) * Fixed.Rad2Deg;
				result.z = Fixed.Atan2(t1, t0) * Fixed.Rad2Deg;

				return result * -1;
			}
		}

		public static Fixed Angle(FQuaternion a, FQuaternion b)
		{
			FQuaternion aInv = FQuaternion.Inverse(a);
			FQuaternion f = b * aInv;

			Fixed angle = Fixed.Acos(f.w) * 2 * Fixed.Rad2Deg;

			if (angle > 180)
			{
				angle = 360 - angle;
			}

			return angle;
		}

		/// <summary>
		/// Quaternions are added.
		/// </summary>
		/// <param name="quaternion1">The first quaternion.</param>
		/// <param name="quaternion2">The second quaternion.</param>
		/// <returns>The sum of both quaternions.</returns>
		#region public static JQuaternion Add(JQuaternion quaternion1, JQuaternion quaternion2)
		public static FQuaternion Add(FQuaternion quaternion1, FQuaternion quaternion2)
		{
			FQuaternion result;
			FQuaternion.Add(ref quaternion1, ref quaternion2, out result);
			return result;
		}

		public static FQuaternion LookRotation(FVector3 forward)
		{
			return CreateFromMatrix(FMatrix.LookAt(forward, FVector3.up));
		}

		public static FQuaternion LookRotation(FVector3 forward, FVector3 upwards)
		{
			return CreateFromMatrix(FMatrix.LookAt(forward, upwards));
		}

		public static FQuaternion Slerp(FQuaternion from, FQuaternion to, Fixed t)
		{
			t = FMath.Clamp(t, 0, 1);

			Fixed dot = Dot(from, to);

			if (dot < Fixed.Zero)
			{
				to = Multiply(to, -1);
				dot = -dot;
			}

			Fixed halfTheta = Fixed.Acos(dot);

			return Multiply(Multiply(from, Fixed.Sin((1 - t) * halfTheta)) + Multiply(to, Fixed.Sin(t * halfTheta)), 1 / Fixed.Sin(halfTheta));
		}

		public static FQuaternion RotateTowards(FQuaternion from, FQuaternion to, Fixed maxDegreesDelta)
		{
			Fixed dot = Dot(from, to);

			if (dot < Fixed.Zero)
			{
				to = Multiply(to, -1);
				dot = -dot;
			}

			Fixed halfTheta = Fixed.Acos(dot);
			Fixed theta = halfTheta * 2;

			maxDegreesDelta *= Fixed.Deg2Rad;

			if (maxDegreesDelta >= theta)
			{
				return to;
			}

			maxDegreesDelta /= theta;

			return Multiply(Multiply(from, Fixed.Sin((1 - maxDegreesDelta) * halfTheta)) + Multiply(to, Fixed.Sin(maxDegreesDelta * halfTheta)), 1 / Fixed.Sin(halfTheta));
		}

		public static FQuaternion Euler(Fixed x, Fixed y, Fixed z)
		{
			x *= Fixed.Deg2Rad;
			y *= Fixed.Deg2Rad;
			z *= Fixed.Deg2Rad;

			FQuaternion rotation;
			FQuaternion.CreateFromYawPitchRoll(y, x, z, out rotation);

			return rotation;
		}

		public static FQuaternion Euler(FVector3 eulerAngles)
		{
			return Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
		}

		public static FQuaternion AngleAxis(Fixed angle, FVector3 axis)
		{
			axis = axis * Fixed.Deg2Rad;
			axis.Normalize();

			Fixed halfAngle = angle * Fixed.Deg2Rad * Fixed.Half;

			FQuaternion rotation;
			Fixed sin = Fixed.Sin(halfAngle);

			rotation.x = axis.x * sin;
			rotation.y = axis.y * sin;
			rotation.z = axis.z * sin;
			rotation.w = Fixed.Cos(halfAngle);

			return rotation;
		}

		public static FQuaternion CreateFromYawPitchRoll(Fixed yaw, Fixed pitch, Fixed roll)
		{
			FQuaternion res;
			CreateFromYawPitchRoll(yaw, pitch, roll, out res);
			return res;
		}

		public static void CreateFromYawPitchRoll(Fixed yaw, Fixed pitch, Fixed roll, out FQuaternion result)
		{
			Fixed num9 = roll * Fixed.Half;
			Fixed num6 = Fixed.Sin(num9);
			Fixed num5 = Fixed.Cos(num9);
			Fixed num8 = pitch * Fixed.Half;
			Fixed num4 = Fixed.Sin(num8);
			Fixed num3 = Fixed.Cos(num8);
			Fixed num7 = yaw * Fixed.Half;
			Fixed num2 = Fixed.Sin(num7);
			Fixed num = Fixed.Cos(num7);
			result.x = ((num * num4) * num5) + ((num2 * num3) * num6);
			result.y = ((num2 * num3) * num5) - ((num * num4) * num6);
			result.z = ((num * num3) * num6) - ((num2 * num4) * num5);
			result.w = ((num * num3) * num5) + ((num2 * num4) * num6);
		}

		/// <summary>
		/// Quaternions are added.
		/// </summary>
		/// <param name="quaternion1">The first quaternion.</param>
		/// <param name="quaternion2">The second quaternion.</param>
		/// <param name="result">The sum of both quaternions.</param>
		public static void Add(ref FQuaternion quaternion1, ref FQuaternion quaternion2, out FQuaternion result)
		{
			result.x = quaternion1.x + quaternion2.x;
			result.y = quaternion1.y + quaternion2.y;
			result.z = quaternion1.z + quaternion2.z;
			result.w = quaternion1.w + quaternion2.w;
		}
		#endregion

		public static FQuaternion Conjugate(FQuaternion value)
		{
			FQuaternion quaternion;
			quaternion.x = -value.x;
			quaternion.y = -value.y;
			quaternion.z = -value.z;
			quaternion.w = value.w;
			return quaternion;
		}

		public static Fixed Dot(FQuaternion a, FQuaternion b)
		{
			return a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z;
		}

		public static FQuaternion Inverse(FQuaternion rotation)
		{
			Fixed invNorm = Fixed.One / ((rotation.x * rotation.x) + (rotation.y * rotation.y) + (rotation.z * rotation.z) + (rotation.w * rotation.w));
			return FQuaternion.Multiply(FQuaternion.Conjugate(rotation), invNorm);
		}

		public static FQuaternion FromToRotation(FVector3 fromVector, FVector3 toVector)
		{
			FVector3 w = FVector3.Cross(fromVector, toVector);
			FQuaternion q = new FQuaternion(w.x, w.y, w.z, FVector3.Dot(fromVector, toVector));
			q.w += Fixed.Sqrt(fromVector.sqrMagnitude * toVector.sqrMagnitude);
			q.Normalize();

			return q;
		}

		public static FQuaternion Lerp(FQuaternion a, FQuaternion b, Fixed t)
		{
			t = FMath.Clamp(t, Fixed.Zero, Fixed.One);

			return LerpUnclamped(a, b, t);
		}

		public static FQuaternion LerpUnclamped(FQuaternion a, FQuaternion b, Fixed t)
		{
			FQuaternion result = FQuaternion.Multiply(a, (1 - t)) + FQuaternion.Multiply(b, t);
			result.Normalize();

			return result;
		}

		/// <summary>
		/// Quaternions are subtracted.
		/// </summary>
		/// <param name="quaternion1">The first quaternion.</param>
		/// <param name="quaternion2">The second quaternion.</param>
		/// <returns>The difference of both quaternions.</returns>
		#region public static JQuaternion Subtract(JQuaternion quaternion1, JQuaternion quaternion2)
		public static FQuaternion Subtract(FQuaternion quaternion1, FQuaternion quaternion2)
		{
			FQuaternion result;
			FQuaternion.Subtract(ref quaternion1, ref quaternion2, out result);
			return result;
		}

		/// <summary>
		/// Quaternions are subtracted.
		/// </summary>
		/// <param name="quaternion1">The first quaternion.</param>
		/// <param name="quaternion2">The second quaternion.</param>
		/// <param name="result">The difference of both quaternions.</param>
		public static void Subtract(ref FQuaternion quaternion1, ref FQuaternion quaternion2, out FQuaternion result)
		{
			result.x = quaternion1.x - quaternion2.x;
			result.y = quaternion1.y - quaternion2.y;
			result.z = quaternion1.z - quaternion2.z;
			result.w = quaternion1.w - quaternion2.w;
		}
		#endregion

		/// <summary>
		/// Multiply two quaternions.
		/// </summary>
		/// <param name="quaternion1">The first quaternion.</param>
		/// <param name="quaternion2">The second quaternion.</param>
		/// <returns>The product of both quaternions.</returns>
		#region public static JQuaternion Multiply(JQuaternion quaternion1, JQuaternion quaternion2)
		public static FQuaternion Multiply(FQuaternion quaternion1, FQuaternion quaternion2)
		{
			FQuaternion result;
			FQuaternion.Multiply(ref quaternion1, ref quaternion2, out result);
			return result;
		}

		/// <summary>
		/// Multiply two quaternions.
		/// </summary>
		/// <param name="quaternion1">The first quaternion.</param>
		/// <param name="quaternion2">The second quaternion.</param>
		/// <param name="result">The product of both quaternions.</param>
		public static void Multiply(ref FQuaternion quaternion1, ref FQuaternion quaternion2, out FQuaternion result)
		{
			Fixed x = quaternion1.x;
			Fixed y = quaternion1.y;
			Fixed z = quaternion1.z;
			Fixed w = quaternion1.w;
			Fixed num4 = quaternion2.x;
			Fixed num3 = quaternion2.y;
			Fixed num2 = quaternion2.z;
			Fixed num = quaternion2.w;
			Fixed num12 = (y * num2) - (z * num3);
			Fixed num11 = (z * num4) - (x * num2);
			Fixed num10 = (x * num3) - (y * num4);
			Fixed num9 = ((x * num4) + (y * num3)) + (z * num2);
			result.x = ((x * num) + (num4 * w)) + num12;
			result.y = ((y * num) + (num3 * w)) + num11;
			result.z = ((z * num) + (num2 * w)) + num10;
			result.w = (w * num) - num9;
		}
		#endregion

		/// <summary>
		/// Scale a quaternion
		/// </summary>
		/// <param name="quaternion1">The quaternion to scale.</param>
		/// <param name="scaleFactor">Scale factor.</param>
		/// <returns>The scaled quaternion.</returns>
		#region public static JQuaternion Multiply(JQuaternion quaternion1, Fixed scaleFactor)
		public static FQuaternion Multiply(FQuaternion quaternion1, Fixed scaleFactor)
		{
			FQuaternion result;
			FQuaternion.Multiply(ref quaternion1, scaleFactor, out result);
			return result;
		}

		/// <summary>
		/// Scale a quaternion
		/// </summary>
		/// <param name="quaternion1">The quaternion to scale.</param>
		/// <param name="scaleFactor">Scale factor.</param>
		/// <param name="result">The scaled quaternion.</param>
		public static void Multiply(ref FQuaternion quaternion1, Fixed scaleFactor, out FQuaternion result)
		{
			result.x = quaternion1.x * scaleFactor;
			result.y = quaternion1.y * scaleFactor;
			result.z = quaternion1.z * scaleFactor;
			result.w = quaternion1.w * scaleFactor;
		}
		#endregion

		/// <summary>
		/// Sets the length of the quaternion to one.
		/// </summary>
		#region public void Normalize()
		public void Normalize()
		{
			Fixed num2 = (((this.x * this.x) + (this.y * this.y)) + (this.z * this.z)) + (this.w * this.w);
			Fixed num = 1 / (Fixed.Sqrt(num2));
			this.x *= num;
			this.y *= num;
			this.z *= num;
			this.w *= num;
		}
		#endregion

		/// <summary>
		/// Creates a quaternion from a matrix.
		/// </summary>
		/// <param name="matrix">A matrix representing an orientation.</param>
		/// <returns>JQuaternion representing an orientation.</returns>
		#region public static JQuaternion CreateFromMatrix(JMatrix matrix)
		public static FQuaternion CreateFromMatrix(FMatrix matrix)
		{
			FQuaternion result;
			FQuaternion.CreateFromMatrix(ref matrix, out result);
			return result;
		}

		/// <summary>
		/// Creates a quaternion from a matrix.
		/// </summary>
		/// <param name="matrix">A matrix representing an orientation.</param>
		/// <param name="result">JQuaternion representing an orientation.</param>
		public static void CreateFromMatrix(ref FMatrix matrix, out FQuaternion result)
		{
			Fixed num8 = (matrix.M11 + matrix.M22) + matrix.M33;
			if (num8 > Fixed.Zero)
			{
				Fixed num = Fixed.Sqrt((num8 + Fixed.One));
				result.w = num * Fixed.Half;
				num = Fixed.Half / num;
				result.x = (matrix.M23 - matrix.M32) * num;
				result.y = (matrix.M31 - matrix.M13) * num;
				result.z = (matrix.M12 - matrix.M21) * num;
			}
			else if ((matrix.M11 >= matrix.M22) && (matrix.M11 >= matrix.M33))
			{
				Fixed num7 = Fixed.Sqrt((((Fixed.One + matrix.M11) - matrix.M22) - matrix.M33));
				Fixed num4 = Fixed.Half / num7;
				result.x = Fixed.Half * num7;
				result.y = (matrix.M12 + matrix.M21) * num4;
				result.z = (matrix.M13 + matrix.M31) * num4;
				result.w = (matrix.M23 - matrix.M32) * num4;
			}
			else if (matrix.M22 > matrix.M33)
			{
				Fixed num6 = Fixed.Sqrt((((Fixed.One + matrix.M22) - matrix.M11) - matrix.M33));
				Fixed num3 = Fixed.Half / num6;
				result.x = (matrix.M21 + matrix.M12) * num3;
				result.y = Fixed.Half * num6;
				result.z = (matrix.M32 + matrix.M23) * num3;
				result.w = (matrix.M31 - matrix.M13) * num3;
			}
			else
			{
				Fixed num5 = Fixed.Sqrt((((Fixed.One + matrix.M33) - matrix.M11) - matrix.M22));
				Fixed num2 = Fixed.Half / num5;
				result.x = (matrix.M31 + matrix.M13) * num2;
				result.y = (matrix.M32 + matrix.M23) * num2;
				result.z = Fixed.Half * num5;
				result.w = (matrix.M12 - matrix.M21) * num2;
			}
		}
		#endregion

		/// <summary>
		/// Multiply two quaternions.
		/// </summary>
		/// <param name="value1">The first quaternion.</param>
		/// <param name="value2">The second quaternion.</param>
		/// <returns>The product of both quaternions.</returns>
		#region public static Fixed operator *(JQuaternion value1, JQuaternion value2)
		public static FQuaternion operator *(FQuaternion value1, FQuaternion value2)
		{
			FQuaternion result;
			FQuaternion.Multiply(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Add two quaternions.
		/// </summary>
		/// <param name="value1">The first quaternion.</param>
		/// <param name="value2">The second quaternion.</param>
		/// <returns>The sum of both quaternions.</returns>
		#region public static Fixed operator +(JQuaternion value1, JQuaternion value2)
		public static FQuaternion operator +(FQuaternion value1, FQuaternion value2)
		{
			FQuaternion result;
			FQuaternion.Add(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Subtract two quaternions.
		/// </summary>
		/// <param name="value1">The first quaternion.</param>
		/// <param name="value2">The second quaternion.</param>
		/// <returns>The difference of both quaternions.</returns>
		#region public static Fixed operator -(JQuaternion value1, JQuaternion value2)
		public static FQuaternion operator -(FQuaternion value1, FQuaternion value2)
		{
			FQuaternion result;
			FQuaternion.Subtract(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Gets the hashcode of the vector.
		/// </summary>
		/// <returns>Returns the hashcode of the vector.</returns>
		#region public override int GetHashCode()
		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}
		#endregion

		/// <summary>
		/// Tests if an object is equal to this vector.
		/// </summary>
		/// <param name="obj">The object to test.</param>
		/// <returns>Returns true if they are euqal, otherwise false.</returns>
		#region public override bool Equals(object obj)
		public override bool Equals(object obj)
		{
			if (!(obj is FQuaternion)) return false;
			FQuaternion other = (FQuaternion)obj;

			return (((x == other.x) && (y == other.y)) && (z == other.z) && (w == other.w));
		}
		#endregion

		/// <summary>
		/// Tests if two FQuaternion are equal.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>Returns true if both values are equal, otherwise false.</returns>
		#region public static bool operator ==(JVector value1, JVector value2)
		public static bool operator ==(FQuaternion value1, FQuaternion value2)
		{
			return (((value1.x == value2.x) && (value1.y == value2.y)) && (value1.z == value2.z) && (value1.w == value2.w));
		}
		#endregion

		/// <summary>
		/// Tests if two FQuaternion are not equal.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>Returns false if both values are equal, otherwise true.</returns>
		#region public static bool operator !=(FQuaternion value1, FQuaternion value2)
		public static bool operator !=(FQuaternion value1, FQuaternion value2)
		{
			if ((value1.x == value2.x) && (value1.y == value2.y) && (value1.z == value2.z))
			{
				return (value1.w != value2.w);
			}
			return true;
		}
		#endregion

		/**
         *  @brief Rotates a {@link FVector3} by the {@link TSQuanternion}.
         **/
		public static FVector3 operator *(FQuaternion quat, FVector3 vec)
		{
			Fixed num = quat.x * 2f;
			Fixed num2 = quat.y * 2f;
			Fixed num3 = quat.z * 2f;
			Fixed num4 = quat.x * num;
			Fixed num5 = quat.y * num2;
			Fixed num6 = quat.z * num3;
			Fixed num7 = quat.x * num2;
			Fixed num8 = quat.x * num3;
			Fixed num9 = quat.y * num3;
			Fixed num10 = quat.w * num;
			Fixed num11 = quat.w * num2;
			Fixed num12 = quat.w * num3;

			FVector3 result;
			result.x = (1f - (num5 + num6)) * vec.x + (num7 - num12) * vec.y + (num8 + num11) * vec.z;
			result.y = (num7 + num12) * vec.x + (1f - (num4 + num6)) * vec.y + (num9 - num10) * vec.z;
			result.z = (num8 - num11) * vec.x + (num9 + num10) * vec.y + (1f - (num4 + num5)) * vec.z;

			return result;
		}

		public override string ToString()
		{
			return string.Format("({0:f1}, {1:f1}, {2:f1}, {3:f1})", x.AsFloat(), y.AsFloat(), z.AsFloat(), w.AsFloat());
		}
	}
}
