﻿using System;

namespace FixedPointMath
{

	/**
     *  @brief Generates random numbers based on a deterministic approach.
     **/
	public class FRandom
	{
		// From http://www.codeproject.com/Articles/164087/Random-Number-Generation
		// Class FRandom generates random numbers
		// from a uniform distribution using the Mersenne
		// Twister algorithm.
		private const int N = 624;
		private const int M = 397;
		private const uint MATRIX_A = 0x9908b0dfU;
		private const uint UPPER_MASK = 0x80000000U;
		private const uint LOWER_MASK = 0x7fffffffU;
		private const int MAX_RAND_INT = 0x7fffffff;
		private uint[] mag01 = { 0x0U, MATRIX_A };
		private uint[] mt = new uint[N];
		private int mti = N + 1;

		/**
         *  @brief Generates a new instance based on a given seed.
         **/
		public static FRandom New(int seed)
		{
			FRandom r = new FRandom(seed);
			return r;
		}

		private FRandom(int seed)
		{
			init_genrand((uint)seed);
		}

		// current index of the random (needed for sync check)
		public int Index { get { return mti; } }

		public const int MaxRandomInt = 0x7fffffff;
		public readonly static Fixed OneOverMax = (Fixed.One / (Fixed)MaxRandomInt);

		/**
         *  @brief Returns a random integer.
         **/
		public int Next()
		{
			return genrand_int31();
		}

		/**
        *  @brief Returns a random integer.
        **/
		public Fixed NextF64()
		{
			return genrand_F64();
		}

		/**
         *  @brief Returns a integer between a min value [inclusive] and a max value [exclusive].
         **/
		public int Next(int minValue, int maxValue)
		{
			if (minValue > maxValue)
			{
				int tmp = maxValue;
				maxValue = minValue;
				minValue = tmp;
			}

			int range = maxValue - minValue;

			return minValue + Next() % range;
		}

		/**
         *  @brief Returns a {@link Fixed} between a min value [inclusive] and a max value [inclusive].
         **/
		public Fixed Next(float minValue, float maxValue)
		{
			var r = NextF64();
			return FMath.Lerp(minValue, maxValue, r);
		}


		private void init_genrand(uint s)
		{
			mt[0] = s & 0xffffffffU;
			for (mti = 1; mti < N; mti++)
			{
				mt[mti] = (uint)(1812433253U * (mt[mti - 1] ^ (mt[mti - 1] >> 30)) + mti);
				mt[mti] &= 0xffffffffU;
			}
		}

		uint genrand_int32()
		{
			uint y;
			if (mti >= N)
			{
				int kk;
				if (mti == N + 1)
					init_genrand(5489U);
				for (kk = 0; kk < N - M; kk++)
				{
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1U];
				}
				for (; kk < N - 1; kk++)
				{
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1U];
				}
				y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
				mt[N - 1] = mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1U];
				mti = 0;
			}
			y = mt[mti++];
			y ^= (y >> 11);
			y ^= (y << 7) & 0x9d2c5680U;
			y ^= (y << 15) & 0xefc60000U;
			y ^= (y >> 18);
			return y;
		}

		private int genrand_int31()
		{
			return (int)(genrand_int32() >> 1);
		}

		Fixed genrand_F64()
		{
			return (Fixed)genrand_int31() * OneOverMax;
		}

	}

}
