﻿/* Copyright (C) <2009-2011> <Thorben Linneweber, Jitter Physics>
*
*  This software is provided 'as-is', without any express or implied
*  warranty.  In no event will the authors be held liable for any damages
*  arising from the use of this software.
*
*  Permission is granted to anyone to use this software for any purpose,
*  including commercial applications, and to alter it and redistribute it
*  freely, subject to the following restrictions:
*
*  1. The origin of this software must not be misrepresented; you must not
*      claim that you wrote the original software. If you use this software
*      in a product, an acknowledgment in the product documentation would be
*      appreciated but is not required.
*  2. Altered source versions must be plainly marked as such, and must not be
*      misrepresented as being the original software.
*  3. This notice may not be removed or altered from any source distribution.
*/

using System;


namespace FixedPointMath
{
	/// <summary>
	/// A vector structure.
	/// </summary>
	[Serializable]
	public struct FVector4
	{
		private static Fixed ZeroEpsilonSq = Fixed.Epsilon;

		/// <summary>The X component of the vector.</summary>
		public Fixed x;
		/// <summary>The Y component of the vector.</summary>
		public Fixed y;
		/// <summary>The Z component of the vector.</summary>
		public Fixed z;
		/// <summary>The W component of the vector.</summary>
		public Fixed w;

		#region Static readonly variables
		/// <summary>
		/// A vector with components (0,0,0);
		/// </summary>
		public static readonly FVector4 zero;
		/// <summary>
		/// A vector with components (1,1,1);
		/// </summary>
		public static readonly FVector4 one;
		/// <summary>
		/// A vector with components
		/// (Fixed.MinValue,Fixed.MinValue,Fixed.MinValue);
		/// </summary>
		public static readonly FVector4 MinValue;
		/// <summary>
		/// A vector with components
		/// (Fixed.MaxValue,Fixed.MaxValue,Fixed.MaxValue);
		/// </summary>
		public static readonly FVector4 MaxValue;
		#endregion

		#region Private static constructor
		static FVector4()
		{
			one = new FVector4(1, 1, 1, 1);
			zero = new FVector4(0, 0, 0, 0);
			MinValue = new FVector4(Fixed.MinValue);
			MaxValue = new FVector4(Fixed.MaxValue);
		}
		#endregion

		public static FVector4 Abs(FVector4 other)
		{
			return new FVector4(Fixed.Abs(other.x), Fixed.Abs(other.y), Fixed.Abs(other.z), Fixed.Abs(other.w));
		}

		/// <summary>
		/// Gets the squared length of the vector.
		/// </summary>
		/// <returns>Returns the squared length of the vector.</returns>
		public Fixed sqrMagnitude
		{
			get
			{
				return (((this.x * this.x) + (this.y * this.y)) + (this.z * this.z) + (this.w * this.w));
			}
		}

		/// <summary>
		/// Gets the length of the vector.
		/// </summary>
		/// <returns>Returns the length of the vector.</returns>
		public Fixed magnitude
		{
			get
			{
				return Fixed.Sqrt(sqrMagnitude);
			}
		}

		public static FVector4 ClampMagnitude(FVector4 vector, Fixed maxLength)
		{
			return Normalize(vector) * maxLength;
		}

		/// <summary>
		/// Gets a normalized version of the vector.
		/// </summary>
		/// <returns>Returns a normalized version of the vector.</returns>
		public FVector4 normalized
		{
			get
			{
				FVector4 result = new FVector4(this.x, this.y, this.z, this.w);
				result.Normalize();

				return result;
			}
		}

		/// <summary>
		/// Constructor initializing a new instance of the structure
		/// </summary>
		/// <param name="x">The X component of the vector.</param>
		/// <param name="y">The Y component of the vector.</param>
		/// <param name="z">The Z component of the vector.</param>
		/// <param name="w">The W component of the vector.</param>

		public FVector4(int x, int y, int z, int w)
		{
			this.x = (Fixed)x;
			this.y = (Fixed)y;
			this.z = (Fixed)z;
			this.w = (Fixed)w;
		}

		public FVector4(Fixed x, Fixed y, Fixed z, Fixed w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		/// <summary>
		/// Multiplies each component of the vector by the same components of the provided vector.
		/// </summary>
		public void Scale(FVector4 other)
		{
			this.x = x * other.x;
			this.y = y * other.y;
			this.z = z * other.z;
		}

		/// <summary>
		/// Sets all vector component to specific values.
		/// </summary>
		/// <param name="x">The X component of the vector.</param>
		/// <param name="y">The Y component of the vector.</param>
		/// <param name="z">The Z component of the vector.</param>
		public void Set(Fixed x, Fixed y, Fixed z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		/// <summary>
		/// Constructor initializing a new instance of the structure
		/// </summary>
		/// <param name="xyzw">All components of the vector are set to xyz</param>
		public FVector4(Fixed xyzw)
		{
			this.x = xyzw;
			this.y = xyzw;
			this.z = xyzw;
			this.w = xyzw;
		}

		public static FVector4 Lerp(FVector4 from, FVector4 to, Fixed percent)
		{
			return from + (to - from) * percent;
		}

		/// <summary>
		/// Builds a string from the JVector.
		/// </summary>
		/// <returns>A string containing all three components.</returns>
		#region public override string ToString()
		public override string ToString()
		{
			return string.Format("({0:f1}, {1:f1}, {2:f1})", x.AsFloat(), y.AsFloat(), z.AsFloat());
		}
		#endregion

		/// <summary>
		/// Tests if an object is equal to this vector.
		/// </summary>
		/// <param name="obj">The object to test.</param>
		/// <returns>Returns true if they are euqal, otherwise false.</returns>
		#region public override bool Equals(object obj)
		public override bool Equals(object obj)
		{
			if (!(obj is FVector4)) return false;
			FVector4 other = (FVector4)obj;

			return (((x == other.x) && (y == other.y)) && (z == other.z) && (w == other.w));
		}
		#endregion

		/// <summary>
		/// Multiplies each component of the vector by the same components of the provided vector.
		/// </summary>
		public static FVector4 Scale(FVector4 vecA, FVector4 vecB)
		{
			FVector4 result;
			result.x = vecA.x * vecB.x;
			result.y = vecA.y * vecB.y;
			result.z = vecA.z * vecB.z;
			result.w = vecA.w * vecB.w;

			return result;
		}

		/// <summary>
		/// Tests if two JVector are equal.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>Returns true if both values are equal, otherwise false.</returns>
		#region public static bool operator ==(JVector value1, JVector value2)
		public static bool operator ==(FVector4 value1, FVector4 value2)
		{
			return (((value1.x == value2.x) && (value1.y == value2.y)) && (value1.z == value2.z) && (value1.w == value2.w));
		}
		#endregion

		/// <summary>
		/// Tests if two JVector are not equal.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>Returns false if both values are equal, otherwise true.</returns>
		#region public static bool operator !=(JVector value1, JVector value2)
		public static bool operator !=(FVector4 value1, FVector4 value2)
		{
			if ((value1.x == value2.x) && (value1.y == value2.y) && (value1.z == value2.z))
			{
				return (value1.w != value2.w);
			}
			return true;
		}
		#endregion

		/// <summary>
		/// Gets a vector with the minimum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>A vector with the minimum x,y and z values of both vectors.</returns>
		#region public static JVector Min(JVector value1, JVector value2)

		public static FVector4 Min(FVector4 value1, FVector4 value2)
		{
			FVector4 result;
			FVector4.Min(ref value1, ref value2, out result);
			return result;
		}

		/// <summary>
		/// Gets a vector with the minimum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <param name="result">A vector with the minimum x,y and z values of both vectors.</param>
		public static void Min(ref FVector4 value1, ref FVector4 value2, out FVector4 result)
		{
			result.x = (value1.x < value2.x) ? value1.x : value2.x;
			result.y = (value1.y < value2.y) ? value1.y : value2.y;
			result.z = (value1.z < value2.z) ? value1.z : value2.z;
			result.w = (value1.w < value2.w) ? value1.w : value2.w;
		}
		#endregion

		/// <summary>
		/// Gets a vector with the maximum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>A vector with the maximum x,y and z values of both vectors.</returns>
		#region public static JVector Max(JVector value1, JVector value2)
		public static FVector4 Max(FVector4 value1, FVector4 value2)
		{
			FVector4 result;
			FVector4.Max(ref value1, ref value2, out result);
			return result;
		}

		public static Fixed Distance(FVector4 v1, FVector4 v2)
		{
			return Fixed.Sqrt((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y) + (v1.z - v2.z) * (v1.z - v2.z) + (v1.w - v2.w) * (v1.w - v2.w));
		}

		/// <summary>
		/// Gets a vector with the maximum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <param name="result">A vector with the maximum x,y and z values of both vectors.</param>
		public static void Max(ref FVector4 value1, ref FVector4 value2, out FVector4 result)
		{
			result.x = (value1.x > value2.x) ? value1.x : value2.x;
			result.y = (value1.y > value2.y) ? value1.y : value2.y;
			result.z = (value1.z > value2.z) ? value1.z : value2.z;
			result.w = (value1.w > value2.w) ? value1.w : value2.w;
		}
		#endregion

		/// <summary>
		/// Sets the length of the vector to zero.
		/// </summary>
		#region public void MakeZero()
		public void MakeZero()
		{
			x = Fixed.Zero;
			y = Fixed.Zero;
			z = Fixed.Zero;
			w = Fixed.Zero;
		}
		#endregion

		/// <summary>
		/// Checks if the length of the vector is zero.
		/// </summary>
		/// <returns>Returns true if the vector is zero, otherwise false.</returns>
		#region public bool IsZero()
		public bool IsZero()
		{
			return (this.sqrMagnitude == Fixed.Zero);
		}

		/// <summary>
		/// Checks if the length of the vector is nearly zero.
		/// </summary>
		/// <returns>Returns true if the vector is nearly zero, otherwise false.</returns>
		public bool IsNearlyZero()
		{
			return (this.sqrMagnitude < ZeroEpsilonSq);
		}
		#endregion

		/// <summary>
		/// Calculates the dot product of two vectors.
		/// </summary>
		/// <param name="vector1">The first vector.</param>
		/// <param name="vector2">The second vector.</param>
		/// <returns>Returns the dot product of both vectors.</returns>
		#region public static Fixed Dot(JVector vector1, JVector vector2)
		public static Fixed Dot(FVector4 vector1, FVector4 vector2)
		{
			return FVector4.Dot(ref vector1, ref vector2);
		}


		/// <summary>
		/// Calculates the dot product of both vectors.
		/// </summary>
		/// <param name="vector1">The first vector.</param>
		/// <param name="vector2">The second vector.</param>
		/// <returns>Returns the dot product of both vectors.</returns>
		public static Fixed Dot(ref FVector4 vector1, ref FVector4 vector2)
		{
			return ((vector1.x * vector2.x) + (vector1.y * vector2.y)) + (vector1.z * vector2.z) + (vector1.w * vector2.w);
		}
		#endregion

		/// <summary>
		/// Adds two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The sum of both vectors.</returns>
		#region public static void Add(JVector value1, JVector value2)
		public static FVector4 Add(FVector4 value1, FVector4 value2)
		{
			FVector4 result;
			FVector4.Add(ref value1, ref value2, out result);
			return result;
		}

		/// <summary>
		/// Adds to vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <param name="result">The sum of both vectors.</param>
		public static void Add(ref FVector4 value1, ref FVector4 value2, out FVector4 result)
		{
			Fixed num0 = value1.x + value2.x;
			Fixed num1 = value1.y + value2.y;
			Fixed num2 = value1.z + value2.z;
			Fixed num3 = value1.w + value2.w;

			result.x = num0;
			result.y = num1;
			result.z = num2;
			result.w = num3;
		}
		#endregion

		/// <summary>
		/// Divides a vector by a factor.
		/// </summary>
		/// <param name="value1">The vector to divide.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		public static FVector4 Divide(FVector4 value1, Fixed scaleFactor)
		{
			FVector4 result;
			FVector4.Divide(ref value1, scaleFactor, out result);
			return result;
		}

		/// <summary>
		/// Divides a vector by a factor.
		/// </summary>
		/// <param name="value1">The vector to divide.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <param name="result">Returns the scaled vector.</param>
		public static void Divide(ref FVector4 value1, Fixed scaleFactor, out FVector4 result)
		{
			result.x = value1.x / scaleFactor;
			result.y = value1.y / scaleFactor;
			result.z = value1.z / scaleFactor;
			result.w = value1.w / scaleFactor;
		}

		/// <summary>
		/// Subtracts two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The difference of both vectors.</returns>
		#region public static JVector Subtract(JVector value1, JVector value2)
		public static FVector4 Subtract(FVector4 value1, FVector4 value2)
		{
			FVector4 result;
			FVector4.Subtract(ref value1, ref value2, out result);
			return result;
		}

		/// <summary>
		/// Subtracts to vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <param name="result">The difference of both vectors.</param>
		public static void Subtract(ref FVector4 value1, ref FVector4 value2, out FVector4 result)
		{
			Fixed num0 = value1.x - value2.x;
			Fixed num1 = value1.y - value2.y;
			Fixed num2 = value1.z - value2.z;
			Fixed num3 = value1.w - value2.w;

			result.x = num0;
			result.y = num1;
			result.z = num2;
			result.w = num3;
		}
		#endregion

		/// <summary>
		/// Gets the hashcode of the vector.
		/// </summary>
		/// <returns>Returns the hashcode of the vector.</returns>
		#region public override int GetHashCode()
		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}
		#endregion

		/// <summary>
		/// Inverses the direction of the vector.
		/// </summary>
		#region public static JVector Negate(JVector value)
		public void Negate()
		{
			this.x = -this.x;
			this.y = -this.y;
			this.z = -this.z;
			this.w = -this.w;
		}

		/// <summary>
		/// Inverses the direction of a vector.
		/// </summary>
		/// <param name="value">The vector to inverse.</param>
		/// <returns>The negated vector.</returns>
		public static FVector4 Negate(FVector4 value)
		{
			FVector4 result;
			FVector4.Negate(ref value, out result);
			return result;
		}

		/// <summary>
		/// Inverses the direction of a vector.
		/// </summary>
		/// <param name="value">The vector to inverse.</param>
		/// <param name="result">The negated vector.</param>
		public static void Negate(ref FVector4 value, out FVector4 result)
		{
			Fixed num0 = -value.x;
			Fixed num1 = -value.y;
			Fixed num2 = -value.z;
			Fixed num3 = -value.w;

			result.x = num0;
			result.y = num1;
			result.z = num2;
			result.w = num3;
		}
		#endregion

		/// <summary>
		/// Normalizes the given vector.
		/// </summary>
		/// <param name="value">The vector which should be normalized.</param>
		/// <returns>A normalized vector.</returns>
		#region public static JVector Normalize(JVector value)
		public static FVector4 Normalize(FVector4 value)
		{
			FVector4 result;
			FVector4.Normalize(ref value, out result);
			return result;
		}

		/// <summary>
		/// Normalizes this vector.
		/// </summary>
		public void Normalize()
		{
			Fixed num2 = sqrMagnitude;
			Fixed num = Fixed.One / Fixed.Sqrt(num2);
			this.x *= num;
			this.y *= num;
			this.z *= num;
			this.w *= num;
		}

		/// <summary>
		/// Normalizes the given vector.
		/// </summary>
		/// <param name="value">The vector which should be normalized.</param>
		/// <param name="result">A normalized vector.</param>
		public static void Normalize(ref FVector4 value, out FVector4 result)
		{
			Fixed num2 = value.sqrMagnitude;
			Fixed num = Fixed.One / Fixed.Sqrt(num2);
			result.x = value.x * num;
			result.y = value.y * num;
			result.z = value.z * num;
			result.w = value.w * num;
		}
		#endregion

		/// <summary>
		/// Multiply a vector with a factor.
		/// </summary>
		/// <param name="value1">The vector to multiply.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <returns>Returns the multiplied vector.</returns>
		#region public static JVector Multiply(JVector value1, Fixed scaleFactor)
		public static FVector4 Multiply(FVector4 value1, Fixed scaleFactor)
		{
			FVector4 result;
			FVector4.Multiply(ref value1, scaleFactor, out result);
			return result;
		}

		/// <summary>
		/// Multiply a vector with a factor.
		/// </summary>
		/// <param name="value1">The vector to multiply.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <param name="result">Returns the multiplied vector.</param>
		public static void Multiply(ref FVector4 value1, Fixed scaleFactor, out FVector4 result)
		{
			result.x = value1.x * scaleFactor;
			result.y = value1.y * scaleFactor;
			result.z = value1.z * scaleFactor;
			result.w = value1.w * scaleFactor;
		}
		#endregion


		/// <summary>
		/// Calculates the dot product of two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>Returns the dot product of both.</returns>
		#region public static Fixed operator *(JVector value1, JVector value2)
		public static Fixed operator *(FVector4 value1, FVector4 value2)
		{
			return FVector4.Dot(ref value1, ref value2);
		}
		#endregion

		/// <summary>
		/// Multiplies a vector by a scale factor.
		/// </summary>
		/// <param name="value1">The vector to scale.</param>
		/// <param name="value2">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		#region public static JVector operator *(JVector value1, Fixed value2)
		public static FVector4 operator *(FVector4 value1, Fixed value2)
		{
			FVector4 result;
			FVector4.Multiply(ref value1, value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Multiplies a vector by a scale factor.
		/// </summary>
		/// <param name="value2">The vector to scale.</param>
		/// <param name="value1">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		#region public static JVector operator *(Fixed value1, JVector value2)
		public static FVector4 operator *(Fixed value1, FVector4 value2)
		{
			FVector4 result;
			FVector4.Multiply(ref value2, value1, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Subtracts two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The difference of both vectors.</returns>
		#region public static JVector operator -(JVector value1, JVector value2)
		public static FVector4 operator -(FVector4 value1, FVector4 value2)
		{
			FVector4 result; FVector4.Subtract(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Adds two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The sum of both vectors.</returns>
		#region public static JVector operator +(JVector value1, JVector value2)
		public static FVector4 operator +(FVector4 value1, FVector4 value2)
		{
			FVector4 result; FVector4.Add(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Divides a vector by a factor.
		/// </summary>
		/// <param name="value1">The vector to divide.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		public static FVector4 operator /(FVector4 value1, Fixed value2)
		{
			FVector4 result;
			FVector4.Divide(ref value1, value2, out result);
			return result;
		}

		public static Fixed Angle(FVector4 a, FVector4 b)
		{
			return Fixed.Acos(a.normalized * b.normalized) * Fixed.Rad2Deg;
		}

		public FVector3 ToFVector3()
		{
			return new FVector3(this.x, this.y, this.z);
		}

	}

}
