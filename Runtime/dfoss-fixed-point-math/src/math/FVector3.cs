﻿/* Copyright (C) <2009-2011> <Thorben Linneweber, Jitter Physics>
*
*  This software is provided 'as-is', without any express or implied
*  warranty.  In no event will the authors be held liable for any damages
*  arising from the use of this software.
*
*  Permission is granted to anyone to use this software for any purpose,
*  including commercial applications, and to alter it and redistribute it
*  freely, subject to the following restrictions:
*
*  1. The origin of this software must not be misrepresented; you must not
*      claim that you wrote the original software. If you use this software
*      in a product, an acknowledgment in the product documentation would be
*      appreciated but is not required.
*  2. Altered source versions must be plainly marked as such, and must not be
*      misrepresented as being the original software.
*  3. This notice may not be removed or altered from any source distribution.
*/

using System;


namespace FixedPointMath
{
	/// <summary>
	/// A vector structure.
	/// </summary>
	[Serializable]
	public struct FVector3
	{
		private static readonly Fixed ZeroEpsilonSq = Fixed.Epsilon;

		/// <summary>The X component of the vector.</summary>
		public Fixed x;
		/// <summary>The Y component of the vector.</summary>
		public Fixed y;
		/// <summary>The Z component of the vector.</summary>
		public Fixed z;

		#region Static readonly variables
		/// <summary>
		/// A vector with components (0,0,0);
		/// </summary>
		public static readonly FVector3 zero;
		/// <summary>
		/// A vector with components (-1,0,0);
		/// </summary>
		public static readonly FVector3 left;
		/// <summary>
		/// A vector with components (1,0,0);
		/// </summary>
		public static readonly FVector3 right;
		/// <summary>
		/// A vector with components (0,1,0);
		/// </summary>
		public static readonly FVector3 up;
		/// <summary>
		/// A vector with components (0,-1,0);
		/// </summary>
		public static readonly FVector3 down;
		/// <summary>
		/// A vector with components (0,0,-1);
		/// </summary>
		public static readonly FVector3 back;
		/// <summary>
		/// A vector with components (0,0,1);
		/// </summary>
		public static readonly FVector3 forward;
		/// <summary>
		/// A vector with components (1,1,1);
		/// </summary>
		public static readonly FVector3 one;
		/// <summary>
		/// A vector with components
		/// (Fixed.MinValue,Fixed.MinValue,Fixed.MinValue);
		/// </summary>
		public static readonly FVector3 MinValue;
		/// <summary>
		/// A vector with components
		/// (Fixed.MaxValue,Fixed.MaxValue,Fixed.MaxValue);
		/// </summary>
		public static readonly FVector3 MaxValue;
		#endregion

		#region Private static constructor
		static FVector3()
		{
			one = new FVector3(1, 1, 1);
			zero = new FVector3(0, 0, 0);
			left = new FVector3(-1, 0, 0);
			right = new FVector3(1, 0, 0);
			up = new FVector3(0, 1, 0);
			down = new FVector3(0, -1, 0);
			back = new FVector3(0, 0, -1);
			forward = new FVector3(0, 0, 1);
			MinValue = new FVector3(Fixed.MinValue);
			MaxValue = new FVector3(Fixed.MaxValue);
		}
		#endregion

		public static FVector3 Abs(FVector3 other)
		{
			return new FVector3(Fixed.Abs(other.x), Fixed.Abs(other.y), Fixed.Abs(other.z));
		}

		/// <summary>
		/// Gets the squared length of the vector.
		/// </summary>
		/// <returns>Returns the squared length of the vector.</returns>
		public Fixed sqrMagnitude
		{
			get
			{
				return (((this.x * this.x) + (this.y * this.y)) + (this.z * this.z));
			}
		}

		/// <summary>
		/// Gets the length of the vector.
		/// </summary>
		/// <returns>Returns the length of the vector.</returns>
		public Fixed magnitude
		{
			get
			{
				Fixed num = ((this.x * this.x) + (this.y * this.y)) + (this.z * this.z);
				return Fixed.Sqrt(num);
			}
		}

		public static FVector3 ClampMagnitude(FVector3 vector, Fixed maxLength)
		{
			return Normalize(vector) * maxLength;
		}

		/// <summary>
		/// Gets a normalized version of the vector.
		/// </summary>
		/// <returns>Returns a normalized version of the vector.</returns>
		public FVector3 normalized
		{
			get
			{
				FVector3 result = new FVector3(this.x, this.y, this.z);
				result.Normalize();

				return result;
			}
		}

		/// <summary>
		/// Constructor initializing a new instance of the structure
		/// </summary>
		/// <param name="x">The X component of the vector.</param>
		/// <param name="y">The Y component of the vector.</param>
		/// <param name="z">The Z component of the vector.</param>

		public FVector3(int x, int y, int z)
		{
			this.x = (Fixed)x;
			this.y = (Fixed)y;
			this.z = (Fixed)z;
		}

		public FVector3(Fixed x, Fixed y, Fixed z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		/// <summary>
		/// Multiplies each component of the vector by the same components of the provided vector.
		/// </summary>
		public void Scale(FVector3 other)
		{
			this.x = x * other.x;
			this.y = y * other.y;
			this.z = z * other.z;
		}

		/// <summary>
		/// Sets all vector component to specific values.
		/// </summary>
		/// <param name="x">The X component of the vector.</param>
		/// <param name="y">The Y component of the vector.</param>
		/// <param name="z">The Z component of the vector.</param>
		public void Set(Fixed x, Fixed y, Fixed z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		/// <summary>
		/// Constructor initializing a new instance of the structure
		/// </summary>
		/// <param name="xyz">All components of the vector are set to xyz</param>
		public FVector3(Fixed xyz)
		{
			this.x = xyz;
			this.y = xyz;
			this.z = xyz;
		}

		public static FVector3 Lerp(FVector3 from, FVector3 to, Fixed percent)
		{
			return from + (to - from) * percent;
		}

		/// <summary>
		/// Builds a string from the JVector.
		/// </summary>
		/// <returns>A string containing all three components.</returns>
		#region public override string ToString()
		public override string ToString()
		{
			return string.Format("({0:f1}, {1:f1}, {2:f1})", x.AsFloat(), y.AsFloat(), z.AsFloat());
		}
		#endregion

		/// <summary>
		/// Tests if an object is equal to this vector.
		/// </summary>
		/// <param name="obj">The object to test.</param>
		/// <returns>Returns true if they are euqal, otherwise false.</returns>
		#region public override bool Equals(object obj)
		public override bool Equals(object obj)
		{
			if (!(obj is FVector3)) return false;
			FVector3 other = (FVector3)obj;

			return (((x == other.x) && (y == other.y)) && (z == other.z));
		}
		#endregion

		/// <summary>
		/// Multiplies each component of the vector by the same components of the provided vector.
		/// </summary>
		public static FVector3 Scale(FVector3 vecA, FVector3 vecB)
		{
			FVector3 result;
			result.x = vecA.x * vecB.x;
			result.y = vecA.y * vecB.y;
			result.z = vecA.z * vecB.z;

			return result;
		}

		/// <summary>
		/// Tests if two JVector are equal.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>Returns true if both values are equal, otherwise false.</returns>
		#region public static bool operator ==(JVector value1, JVector value2)
		public static bool operator ==(FVector3 value1, FVector3 value2)
		{
			return (((value1.x == value2.x) && (value1.y == value2.y)) && (value1.z == value2.z));
		}
		#endregion

		/// <summary>
		/// Tests if two JVector are not equal.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>Returns false if both values are equal, otherwise true.</returns>
		#region public static bool operator !=(JVector value1, JVector value2)
		public static bool operator !=(FVector3 value1, FVector3 value2)
		{
			if ((value1.x == value2.x) && (value1.y == value2.y))
			{
				return (value1.z != value2.z);
			}
			return true;
		}
		#endregion

		/// <summary>
		/// Gets a vector with the minimum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>A vector with the minimum x,y and z values of both vectors.</returns>
		#region public static JVector Min(JVector value1, JVector value2)

		public static FVector3 Min(FVector3 value1, FVector3 value2)
		{
			FVector3 result;
			FVector3.Min(ref value1, ref value2, out result);
			return result;
		}

		/// <summary>
		/// Gets a vector with the minimum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <param name="result">A vector with the minimum x,y and z values of both vectors.</param>
		public static void Min(ref FVector3 value1, ref FVector3 value2, out FVector3 result)
		{
			result.x = (value1.x < value2.x) ? value1.x : value2.x;
			result.y = (value1.y < value2.y) ? value1.y : value2.y;
			result.z = (value1.z < value2.z) ? value1.z : value2.z;
		}
		#endregion

		/// <summary>
		/// Gets a vector with the maximum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <returns>A vector with the maximum x,y and z values of both vectors.</returns>
		#region public static JVector Max(JVector value1, JVector value2)
		public static FVector3 Max(FVector3 value1, FVector3 value2)
		{
			FVector3 result;
			FVector3.Max(ref value1, ref value2, out result);
			return result;
		}

		public static Fixed Distance(FVector3 v1, FVector3 v2)
		{
			return Fixed.Sqrt((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y) + (v1.z - v2.z) * (v1.z - v2.z));
		}

		public static Fixed DistanceSquared(FVector3 v1, FVector3 v2)
		{
			return ((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y) + (v1.z - v2.z) * (v1.z - v2.z));
		}

		/// <summary>
		/// Gets a vector with the maximum x,y and z values of both vectors.
		/// </summary>
		/// <param name="value1">The first value.</param>
		/// <param name="value2">The second value.</param>
		/// <param name="result">A vector with the maximum x,y and z values of both vectors.</param>
		public static void Max(ref FVector3 value1, ref FVector3 value2, out FVector3 result)
		{
			result.x = (value1.x > value2.x) ? value1.x : value2.x;
			result.y = (value1.y > value2.y) ? value1.y : value2.y;
			result.z = (value1.z > value2.z) ? value1.z : value2.z;
		}
		#endregion

		/// <summary>
		/// Sets the length of the vector to zero.
		/// </summary>
		#region public void MakeZero()
		public void MakeZero()
		{
			x = Fixed.Zero;
			y = Fixed.Zero;
			z = Fixed.Zero;
		}
		#endregion

		/// <summary>
		/// Checks if the length of the vector is zero.
		/// </summary>
		/// <returns>Returns true if the vector is zero, otherwise false.</returns>
		#region public bool IsZero()
		public bool IsZero()
		{
			return (this.sqrMagnitude == Fixed.Zero);
		}

		/// <summary>
		/// Checks if the length of the vector is nearly zero.
		/// </summary>
		/// <returns>Returns true if the vector is nearly zero, otherwise false.</returns>
		public bool IsNearlyZero()
		{
			return (this.sqrMagnitude < ZeroEpsilonSq);
		}
		#endregion

		/// <summary>
		/// Transforms a vector by the given matrix.
		/// </summary>
		/// <param name="position">The vector to transform.</param>
		/// <param name="matrix">The transform matrix.</param>
		/// <returns>The transformed vector.</returns>
		#region public static JVector Transform(JVector position, JMatrix matrix)
		public static FVector3 Transform(FVector3 position, FMatrix matrix)
		{
			FVector3 result;
			FVector3.Transform(ref position, ref matrix, out result);
			return result;
		}

		/// <summary>
		/// Transforms a vector by the given matrix.
		/// </summary>
		/// <param name="position">The vector to transform.</param>
		/// <param name="matrix">The transform matrix.</param>
		/// <param name="result">The transformed vector.</param>
		public static void Transform(ref FVector3 position, ref FMatrix matrix, out FVector3 result)
		{
			Fixed num0 = ((position.x * matrix.M11) + (position.y * matrix.M21)) + (position.z * matrix.M31);
			Fixed num1 = ((position.x * matrix.M12) + (position.y * matrix.M22)) + (position.z * matrix.M32);
			Fixed num2 = ((position.x * matrix.M13) + (position.y * matrix.M23)) + (position.z * matrix.M33);

			result.x = num0;
			result.y = num1;
			result.z = num2;
		}

		/// <summary>
		/// Transforms a vector by the transposed of the given Matrix.
		/// </summary>
		/// <param name="position">The vector to transform.</param>
		/// <param name="matrix">The transform matrix.</param>
		/// <param name="result">The transformed vector.</param>
		public static void TransposedTransform(ref FVector3 position, ref FMatrix matrix, out FVector3 result)
		{
			Fixed num0 = ((position.x * matrix.M11) + (position.y * matrix.M12)) + (position.z * matrix.M13);
			Fixed num1 = ((position.x * matrix.M21) + (position.y * matrix.M22)) + (position.z * matrix.M23);
			Fixed num2 = ((position.x * matrix.M31) + (position.y * matrix.M32)) + (position.z * matrix.M33);

			result.x = num0;
			result.y = num1;
			result.z = num2;
		}
		#endregion

		/// <summary>
		/// Calculates the dot product of two vectors.
		/// </summary>
		/// <param name="vector1">The first vector.</param>
		/// <param name="vector2">The second vector.</param>
		/// <returns>Returns the dot product of both vectors.</returns>
		#region public static Fixed Dot(JVector vector1, JVector vector2)
		public static Fixed Dot(FVector3 vector1, FVector3 vector2)
		{
			return FVector3.Dot(ref vector1, ref vector2);
		}


		/// <summary>
		/// Calculates the dot product of both vectors.
		/// </summary>
		/// <param name="vector1">The first vector.</param>
		/// <param name="vector2">The second vector.</param>
		/// <returns>Returns the dot product of both vectors.</returns>
		public static Fixed Dot(ref FVector3 vector1, ref FVector3 vector2)
		{
			return ((vector1.x * vector2.x) + (vector1.y * vector2.y)) + (vector1.z * vector2.z);
		}
		#endregion

		/// <summary>
		/// Adds two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The sum of both vectors.</returns>
		#region public static void Add(JVector value1, JVector value2)
		public static FVector3 Add(FVector3 value1, FVector3 value2)
		{
			FVector3 result;
			FVector3.Add(ref value1, ref value2, out result);
			return result;
		}

		/// <summary>
		/// Adds to vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <param name="result">The sum of both vectors.</param>
		public static void Add(ref FVector3 value1, ref FVector3 value2, out FVector3 result)
		{
			Fixed num0 = value1.x + value2.x;
			Fixed num1 = value1.y + value2.y;
			Fixed num2 = value1.z + value2.z;

			result.x = num0;
			result.y = num1;
			result.z = num2;
		}
		#endregion

		/// <summary>
		/// Divides a vector by a factor.
		/// </summary>
		/// <param name="value1">The vector to divide.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		public static FVector3 Divide(FVector3 value1, Fixed scaleFactor)
		{
			FVector3 result;
			FVector3.Divide(ref value1, scaleFactor, out result);
			return result;
		}

		/// <summary>
		/// Divides a vector by a factor.
		/// </summary>
		/// <param name="value1">The vector to divide.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <param name="result">Returns the scaled vector.</param>
		public static void Divide(ref FVector3 value1, Fixed scaleFactor, out FVector3 result)
		{
			result.x = value1.x / scaleFactor;
			result.y = value1.y / scaleFactor;
			result.z = value1.z / scaleFactor;
		}

		/// <summary>
		/// Subtracts two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The difference of both vectors.</returns>
		#region public static JVector Subtract(JVector value1, JVector value2)
		public static FVector3 Subtract(FVector3 value1, FVector3 value2)
		{
			FVector3 result;
			FVector3.Subtract(ref value1, ref value2, out result);
			return result;
		}

		/// <summary>
		/// Subtracts to vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <param name="result">The difference of both vectors.</param>
		public static void Subtract(ref FVector3 value1, ref FVector3 value2, out FVector3 result)
		{
			Fixed num0 = value1.x - value2.x;
			Fixed num1 = value1.y - value2.y;
			Fixed num2 = value1.z - value2.z;

			result.x = num0;
			result.y = num1;
			result.z = num2;
		}
		#endregion

		/// <summary>
		/// The cross product of two vectors.
		/// </summary>
		/// <param name="vector1">The first vector.</param>
		/// <param name="vector2">The second vector.</param>
		/// <returns>The cross product of both vectors.</returns>
		#region public static JVector Cross(JVector vector1, JVector vector2)
		public static FVector3 Cross(FVector3 vector1, FVector3 vector2)
		{
			FVector3 result;
			FVector3.Cross(ref vector1, ref vector2, out result);
			return result;
		}

		/// <summary>
		/// The cross product of two vectors.
		/// </summary>
		/// <param name="vector1">The first vector.</param>
		/// <param name="vector2">The second vector.</param>
		/// <param name="result">The cross product of both vectors.</param>
		public static void Cross(ref FVector3 vector1, ref FVector3 vector2, out FVector3 result)
		{
			Fixed num3 = (vector1.y * vector2.z) - (vector1.z * vector2.y);
			Fixed num2 = (vector1.z * vector2.x) - (vector1.x * vector2.z);
			Fixed num = (vector1.x * vector2.y) - (vector1.y * vector2.x);
			result.x = num3;
			result.y = num2;
			result.z = num;
		}
		#endregion

		/// <summary>
		/// Gets the hashcode of the vector.
		/// </summary>
		/// <returns>Returns the hashcode of the vector.</returns>
		#region public override int GetHashCode()
		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}
		#endregion

		/// <summary>
		/// Inverses the direction of the vector.
		/// </summary>
		#region public static JVector Negate(JVector value)
		public void Negate()
		{
			this.x = -this.x;
			this.y = -this.y;
			this.z = -this.z;
		}

		/// <summary>
		/// Inverses the direction of a vector.
		/// </summary>
		/// <param name="value">The vector to inverse.</param>
		/// <returns>The negated vector.</returns>
		public static FVector3 Negate(FVector3 value)
		{
			FVector3 result;
			FVector3.Negate(ref value, out result);
			return result;
		}

		/// <summary>
		/// Inverses the direction of a vector.
		/// </summary>
		/// <param name="value">The vector to inverse.</param>
		/// <param name="result">The negated vector.</param>
		public static void Negate(ref FVector3 value, out FVector3 result)
		{
			Fixed num0 = -value.x;
			Fixed num1 = -value.y;
			Fixed num2 = -value.z;

			result.x = num0;
			result.y = num1;
			result.z = num2;
		}
		#endregion

		/// <summary>
		/// Normalizes the given vector.
		/// </summary>
		/// <param name="value">The vector which should be normalized.</param>
		/// <returns>A normalized vector.</returns>
		#region public static JVector Normalize(JVector value)
		public static FVector3 Normalize(FVector3 value)
		{
			FVector3 result;
			FVector3.Normalize(ref value, out result);
			return result;
		}

		/// <summary>
		/// Normalizes this vector.
		/// </summary>
		public void Normalize()
		{
			Fixed num2 = ((this.x * this.x) + (this.y * this.y)) + (this.z * this.z);
			Fixed num = Fixed.One / Fixed.Sqrt(num2);
			this.x *= num;
			this.y *= num;
			this.z *= num;
		}

		/// <summary>
		/// Normalizes the given vector.
		/// </summary>
		/// <param name="value">The vector which should be normalized.</param>
		/// <param name="result">A normalized vector.</param>
		public static void Normalize(ref FVector3 value, out FVector3 result)
		{
			Fixed num2 = ((value.x * value.x) + (value.y * value.y)) + (value.z * value.z);
			Fixed num = Fixed.One / Fixed.Sqrt(num2);
			result.x = value.x * num;
			result.y = value.y * num;
			result.z = value.z * num;
		}
		#endregion

		#region public static void Swap(ref JVector vector1, ref JVector vector2)

		/// <summary>
		/// Swaps the components of both vectors.
		/// </summary>
		/// <param name="vector1">The first vector to swap with the second.</param>
		/// <param name="vector2">The second vector to swap with the first.</param>
		public static void Swap(ref FVector3 vector1, ref FVector3 vector2)
		{
			Fixed temp;

			temp = vector1.x;
			vector1.x = vector2.x;
			vector2.x = temp;

			temp = vector1.y;
			vector1.y = vector2.y;
			vector2.y = temp;

			temp = vector1.z;
			vector1.z = vector2.z;
			vector2.z = temp;
		}
		#endregion

		/// <summary>
		/// Multiply a vector with a factor.
		/// </summary>
		/// <param name="value1">The vector to multiply.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <returns>Returns the multiplied vector.</returns>
		#region public static JVector Multiply(JVector value1, Fixed scaleFactor)
		public static FVector3 Multiply(FVector3 value1, Fixed scaleFactor)
		{
			FVector3 result;
			FVector3.Multiply(ref value1, scaleFactor, out result);
			return result;
		}

		/// <summary>
		/// Multiply a vector with a factor.
		/// </summary>
		/// <param name="value1">The vector to multiply.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <param name="result">Returns the multiplied vector.</param>
		public static void Multiply(ref FVector3 value1, Fixed scaleFactor, out FVector3 result)
		{
			result.x = value1.x * scaleFactor;
			result.y = value1.y * scaleFactor;
			result.z = value1.z * scaleFactor;
		}
		#endregion

		/// <summary>
		/// Calculates the cross product of two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>Returns the cross product of both.</returns>
		#region public static JVector operator %(JVector value1, JVector value2)
		public static FVector3 operator %(FVector3 value1, FVector3 value2)
		{
			FVector3 result; FVector3.Cross(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Calculates the dot product of two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>Returns the dot product of both.</returns>
		#region public static Fixed operator *(JVector value1, JVector value2)
		public static Fixed operator *(FVector3 value1, FVector3 value2)
		{
			return FVector3.Dot(ref value1, ref value2);
		}
		#endregion

		/// <summary>
		/// Multiplies a vector by a scale factor.
		/// </summary>
		/// <param name="value1">The vector to scale.</param>
		/// <param name="value2">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		#region public static JVector operator *(JVector value1, Fixed value2)
		public static FVector3 operator *(FVector3 value1, Fixed value2)
		{
			FVector3 result;
			FVector3.Multiply(ref value1, value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Multiplies a vector by a scale factor.
		/// </summary>
		/// <param name="value2">The vector to scale.</param>
		/// <param name="value1">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		#region public static JVector operator *(Fixed value1, JVector value2)
		public static FVector3 operator *(Fixed value1, FVector3 value2)
		{
			FVector3 result;
			FVector3.Multiply(ref value2, value1, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Subtracts two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The difference of both vectors.</returns>
		#region public static JVector operator -(JVector value1, JVector value2)
		public static FVector3 operator -(FVector3 value1, FVector3 value2)
		{
			FVector3 result; FVector3.Subtract(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Adds two vectors.
		/// </summary>
		/// <param name="value1">The first vector.</param>
		/// <param name="value2">The second vector.</param>
		/// <returns>The sum of both vectors.</returns>
		#region public static JVector operator +(JVector value1, JVector value2)
		public static FVector3 operator +(FVector3 value1, FVector3 value2)
		{
			FVector3 result; FVector3.Add(ref value1, ref value2, out result);
			return result;
		}
		#endregion

		/// <summary>
		/// Divides a vector by a factor.
		/// </summary>
		/// <param name="value1">The vector to divide.</param>
		/// <param name="scaleFactor">The scale factor.</param>
		/// <returns>Returns the scaled vector.</returns>
		public static FVector3 operator /(FVector3 value1, Fixed value2)
		{
			FVector3 result;
			FVector3.Divide(ref value1, value2, out result);
			return result;
		}

		public static Fixed Angle(FVector3 a, FVector3 b)
		{
			return Fixed.Acos(a.normalized * b.normalized) * Fixed.Rad2Deg;
		}

		public FVector2 ToFVector2()
		{
			return new FVector2(this.x, this.z);
		}

	}

}
