/* Copyright (C) <2009-2011> <Thorben Linneweber, Jitter Physics>
*
*  This software is provided 'as-is', without any express or implied
*  warranty.  In no event will the authors be held liable for any damages
*  arising from the use of this software.
*
*  Permission is granted to anyone to use this software for any purpose,
*  including commercial applications, and to alter it and redistribute it
*  freely, subject to the following restrictions:
*
*  1. The origin of this software must not be misrepresented; you must not
*      claim that you wrote the original software. If you use this software
*      in a product, an acknowledgment in the product documentation would be
*      appreciated but is not required.
*  2. Altered source versions must be plainly marked as such, and must not be
*      misrepresented as being the original software.
*  3. This notice may not be removed or altered from any source distribution.
*/

namespace FixedPointMath
{

	/// <summary>
	/// Contains common math operations.
	/// </summary>
	public sealed class FMath
	{

		/// <summary>
		/// PI constant.
		/// </summary>
		public static Fixed Pi = Fixed.Pi;

		/**
        *  @brief PI over 2 constant.
        **/
		public static Fixed PiOver2 = Fixed.PiOver2;

		/// <summary>
		/// A small value often used to decide if numeric
		/// results are zero.
		/// </summary>
		public static Fixed Epsilon = Fixed.Epsilon;

		/**
        *  @brief Degree to radians constant.
        **/
		public static Fixed Deg2Rad = Fixed.Deg2Rad;

		/**
        *  @brief Radians to degree constant.
        **/
		public static Fixed Rad2Deg = Fixed.Rad2Deg;

		/// <summary>
		/// Gets the square root.
		/// </summary>
		/// <param name="number">The number to get the square root from.</param>
		/// <returns></returns>
		#region public static Fixed Sqrt(Fixed number)
		public static Fixed Sqrt(Fixed number)
		{
			return Fixed.Sqrt(number);
		}
		#endregion

		/// <summary>
		/// Gets the maximum number of two values.
		/// </summary>
		/// <param name="val1">The first value.</param>
		/// <param name="val2">The second value.</param>
		/// <returns>Returns the largest value.</returns>
		#region public static Fixed Max(Fixed val1, Fixed val2)
		public static Fixed Max(Fixed val1, Fixed val2)
		{
			return (val1 > val2) ? val1 : val2;
		}
		#endregion

		/// <summary>
		/// Gets the minimum number of two values.
		/// </summary>
		/// <param name="val1">The first value.</param>
		/// <param name="val2">The second value.</param>
		/// <returns>Returns the smallest value.</returns>
		#region public static Fixed Min(Fixed val1, Fixed val2)
		public static Fixed Min(Fixed val1, Fixed val2)
		{
			return (val1 < val2) ? val1 : val2;
		}
		#endregion

		/// <summary>
		/// Gets the maximum number of three values.
		/// </summary>
		/// <param name="val1">The first value.</param>
		/// <param name="val2">The second value.</param>
		/// <param name="val3">The third value.</param>
		/// <returns>Returns the largest value.</returns>
		#region public static Fixed Max(Fixed val1, Fixed val2,Fixed val3)
		public static Fixed Max(Fixed val1, Fixed val2, Fixed val3)
		{
			Fixed max12 = (val1 > val2) ? val1 : val2;
			return (max12 > val3) ? max12 : val3;
		}
		#endregion

		/// <summary>
		/// Returns a number which is within [min,max]
		/// </summary>
		/// <param name="value">The value to clamp.</param>
		/// <param name="min">The minimum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>The clamped value.</returns>
		#region public static Fixed Clamp(Fixed value, Fixed min, Fixed max)
		public static Fixed Clamp(Fixed value, Fixed min, Fixed max)
		{
			value = (value > max) ? max : value;
			value = (value < min) ? min : value;
			return value;
		}
		#endregion

		/// <summary>
		/// Changes every sign of the matrix entry to '+'
		/// </summary>
		/// <param name="matrix">The matrix.</param>
		/// <param name="result">The absolute matrix.</param>
		#region public static void Absolute(ref JMatrix matrix,out JMatrix result)
		public static void Absolute(ref FMatrix matrix, out FMatrix result)
		{
			result.M11 = Fixed.Abs(matrix.M11);
			result.M12 = Fixed.Abs(matrix.M12);
			result.M13 = Fixed.Abs(matrix.M13);
			result.M21 = Fixed.Abs(matrix.M21);
			result.M22 = Fixed.Abs(matrix.M22);
			result.M23 = Fixed.Abs(matrix.M23);
			result.M31 = Fixed.Abs(matrix.M31);
			result.M32 = Fixed.Abs(matrix.M32);
			result.M33 = Fixed.Abs(matrix.M33);
		}
		#endregion

		/// <summary>
		/// Returns the sine of value.
		/// </summary>
		public static Fixed Sin(Fixed value)
		{
			return Fixed.Sin(value);
		}

		/// <summary>
		/// Returns the cosine of value.
		/// </summary>
		public static Fixed Cos(Fixed value)
		{
			return Fixed.Cos(value);
		}

		/// <summary>
		/// Returns the tan of value.
		/// </summary>
		public static Fixed Tan(Fixed value)
		{
			return Fixed.Tan(value);
		}

		/// <summary>
		/// Returns the arc sine of value.
		/// </summary>
		public static Fixed Asin(Fixed value)
		{
			return Fixed.Asin(value);
		}

		/// <summary>
		/// Returns the arc cosine of value.
		/// </summary>
		public static Fixed Acos(Fixed value)
		{
			return Fixed.Acos(value);
		}

		/// <summary>
		/// Returns the arc tan of value.
		/// </summary>
		public static Fixed Atan(Fixed value)
		{
			return Fixed.Atan(value);
		}

		/// <summary>
		/// Returns the arc tan of coordinates x-y.
		/// </summary>
		public static Fixed Atan2(Fixed y, Fixed x)
		{
			return Fixed.Atan2(y, x);
		}

		/// <summary>
		/// Returns the largest integer less than or equal to the specified number.
		/// </summary>
		public static Fixed Floor(Fixed value)
		{
			return Fixed.Floor(value);
		}

		/// <summary>
		/// Returns the smallest integral value that is greater than or equal to the specified number.
		/// </summary>
		public static Fixed Ceiling(Fixed value)
		{
			return Fixed.Ceiling(value);
		}

		/// <summary>
		/// Rounds a value to the nearest integral value.
		/// If the value is halfway between an even and an uneven value, returns the even value.
		/// </summary>
		public static Fixed Round(Fixed value)
		{
			return Fixed.Round(value);
		}

		/// <summary>
		/// Returns a number indicating the sign of a Fix64 number.
		/// Returns 1 if the value is positive, 0 if is 0, and -1 if it is negative.
		/// </summary>
		public static int Sign(Fixed value)
		{
			return Fixed.Sign(value);
		}

		/// <summary>
		/// Returns the absolute value of a Fix64 number.
		/// Note: Abs(Fix64.MinValue) == Fix64.MaxValue.
		/// </summary>
		public static Fixed Abs(Fixed value)
		{
			return Fixed.Abs(value);
		}

		public static Fixed Barycentric(Fixed value1, Fixed value2, Fixed value3, Fixed amount1, Fixed amount2)
		{
			return value1 + (value2 - value1) * amount1 + (value3 - value1) * amount2;
		}

		public static Fixed CatmullRom(Fixed value1, Fixed value2, Fixed value3, Fixed value4, Fixed amount)
		{
			// Using formula from http://www.mvps.org/directx/articles/catmull/
			// Internally using F64s not to lose precission
			Fixed amountSquared = amount * amount;
			Fixed amountCubed = amountSquared * amount;
			return (Fixed)(0.5f * (2.0f * value2 +
								 (value3 - value1) * amount +
								 (2.0f * value1 - 5.0f * value2 + 4.0f * value3 - value4) * amountSquared +
								 (3.0f * value2 - value1 - 3.0f * value3 + value4) * amountCubed));
		}

		public static Fixed Distance(Fixed value1, Fixed value2)
		{
			return Fixed.Abs(value1 - value2);
		}

		public static Fixed Hermite(Fixed value1, Fixed tangent1, Fixed value2, Fixed tangent2, Fixed amount)
		{
			// All transformed to Fixed not to lose precission
			// Otherwise, for high numbers of param:amount the result is NaN instead of Infinity
			Fixed v1 = value1, v2 = value2, t1 = tangent1, t2 = tangent2, s = amount, result;
			Fixed sCubed = s * s * s;
			Fixed sSquared = s * s;

			if (amount == 0f)
				result = value1;
			else if (amount == 1f)
				result = value2;
			else
				result = (2 * v1 - 2 * v2 + t2 + t1) * sCubed +
						 (3 * v2 - 3 * v1 - 2 * t1 - t2) * sSquared +
						 t1 * s +
						 v1;
			return (Fixed)result;
		}

		public static Fixed Lerp(Fixed value1, Fixed value2, Fixed amount)
		{
			return value1 + (value2 - value1) * amount;
		}

		public static Fixed SmoothStep(Fixed value1, Fixed value2, Fixed amount)
		{
			// It is expected that 0 < amount < 1
			// If amount < 0, return value1
			// If amount > 1, return value2
			Fixed result = Clamp(amount, 0f, 1f);
			result = Hermite(value1, 0f, value2, 0f, result);
			return result;
		}

		// Calculates the Lerp parameter between of two values.
		public static Fixed InverseLerp(Fixed a, Fixed b, Fixed value)
		{
			if (a != b)
				return Clamp((value - a) / (b - a), Fixed.Zero, Fixed.One);
			else
				return Fixed.Zero;
		}

	}
}
